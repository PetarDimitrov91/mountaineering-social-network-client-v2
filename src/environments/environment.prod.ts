export const environment = {
  production: true,
  API_URL: 'https://mountain-app-rest-api.herokuapp.com'
};
