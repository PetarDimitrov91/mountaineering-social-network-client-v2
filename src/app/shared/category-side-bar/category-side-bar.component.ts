import {Component, Input, ViewEncapsulation} from '@angular/core';
import { CategoryService } from 'src/app/main/category.service';
import { CategoryModel } from 'src/app/main/interfaces/category-response';

@Component({
  selector: 'app-category-side-bar',
  templateUrl: './category-side-bar.component.html',
  styleUrls: ['./category-side-bar.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class CategorySideBarComponent{
 @Input() categories: Array<CategoryModel> = [];
  displayViewAll: boolean | undefined;

  constructor(
    private categoryService: CategoryService) {
    this.categoryService.getAllCategories()
      .subscribe((data) => {
      this.categories = data;
    });
  }
}
