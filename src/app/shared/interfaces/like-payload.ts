import { LikeType } from './like-type';

export class LikePayload {
    likeType: LikeType | undefined;
    postId: number | undefined;
}
