export interface PostModel {
    id: number;
    postName: string;
    url: string;
    description: string;
    likesCount: number;
    userName: string;
    categoryName: string;
    commentCount: number;
    duration: string;
    like: boolean;
    unlike: boolean;
    imgByte: [],
}
