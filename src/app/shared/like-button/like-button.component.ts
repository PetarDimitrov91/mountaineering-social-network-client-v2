/* tslint:disable:no-trailing-whitespace */
import {Component, Input, OnInit} from '@angular/core';
import {PostModel} from '../interfaces/post-model';
import {faArrowDown, faArrowUp} from '@fortawesome/free-solid-svg-icons';
import {LikePayload} from '../interfaces/like-payload';
import {LikeType} from '../interfaces/like-type';
import {LikeService} from '../like.service';
import {AuthService} from 'src/app/auth/auth.service';
import {PostService} from '../../main/post.service';
import {throwError} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-like-button',
  templateUrl: './like-button.component.html',
  styleUrls: ['./like-button.component.css']
})
export class LikeButtonComponent implements OnInit {

  @Input() post: PostModel | undefined;
  likePayload: LikePayload;
  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;
  likeColor: string | undefined;
  unlikeColor: string | undefined;
  isLoggedIn:boolean;

  constructor(
    private likeService: LikeService,
    private authService: AuthService,
    private postService: PostService,
    private toastr: ToastrService,
    private router: Router
  ) {

    this.likePayload = {
      likeType: undefined,
      postId: undefined
    };

    this.isLoggedIn=this.authService.isLoggedIn;
  }

  ngOnInit(): void {
    this.updateLikeDetails();
  }

  likePost() {
    if(!this.isLoggedIn){
      this.router.navigate(['/login'])
        .catch(e => console.log(e));

      return;
    }

    this.likePayload.likeType = LikeType.LIKE;
    this.like();
    this.unlikeColor = '';
  }

  unlikePost() {
    if(!this.isLoggedIn){
      this.router.navigate(['/login'])
        .catch(e => console.log(e));

      return;
    }

    this.likePayload.likeType = LikeType.UNLIKE;
    this.like();
    this.likeColor = '';
  }

  private like() {
    this.likePayload.postId = this.post!.id;

    this.likeService.like(this.likePayload)
      .subscribe({next:() => {
      this.updateLikeDetails();
    }, error: (error) => {

      const message:string = this.likePayload.likeType === LikeType.UNLIKE
        ? 'You already unliked this post'
        : 'You already liked this post';

      this.toastr.error(message);
      throwError(error);
    }});
  }

  private updateLikeDetails() {
    this.postService.getPost(this.post!.id)
      .subscribe((post) => {
      this.post = post;
    });
  }
}
