import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {LikeService} from './like.service';
import {PostService} from '../main/post.service';
import {CategorySideBarComponent} from './category-side-bar/category-side-bar.component';
import {LikeButtonComponent} from './like-button/like-button.component';
import {PostCardComponent} from './post-card/post-card.component';
import {SideBarComponent} from './side-bar/side-bar.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {DialogComponent } from './dialog/dialog.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    CategorySideBarComponent,
    LikeButtonComponent,
    PostCardComponent,
    SideBarComponent,
    DialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgbDropdownModule,
    ReactiveFormsModule,
  ],
  providers:[
    LikeService,
    PostService,
  ],
  exports:[
    CategorySideBarComponent,
    LikeButtonComponent,
    PostCardComponent,
    SideBarComponent
  ]
})
export class SharedModule { }
