import {Component, Input, OnInit} from '@angular/core';
import {faComments, faListDots} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {PostModel} from "../interfaces/post-model";
import {PostService} from "../../main/post.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CategoryModel} from "../../main/interfaces/category-response";
import {throwError} from "rxjs";
import {CategoryService} from "../../main/category.service";
import {PostPayload} from "../../main/interfaces/post.payload";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css'],
})
export class PostCardComponent implements OnInit {

  faComments = faComments;
  faArrowDown = faListDots;
  @Input() posts: any[] | undefined;
  @Input() profileContext: boolean | undefined;

  isLoggedIn: boolean;
  loading: boolean;

  showDialogDelete: boolean;
  showDialogEditData: boolean;
  showDialogEditImg: boolean;
  showDialogImageOnClick: boolean;

  editPostDataForm: FormGroup;
  editPostImgForm: FormGroup;
  selectedFile: File | undefined;
  formData = new FormData();

  categories: Array<CategoryModel> | undefined;

  private postToDelete: PostModel | undefined;
  private postToUpdate: PostModel | undefined;
  private postPayload: PostPayload | undefined

  constructor(
    private authService: AuthService,
    private router: Router,
    private postService: PostService,
    private categoryService: CategoryService,
    private toastr: ToastrService
  ) {
    this.isLoggedIn = this.authService.isLoggedIn;
    this.showDialogDelete = false;
    this.showDialogEditData = false;
    this.showDialogEditImg = false;
    this.showDialogImageOnClick = false;
    this.loading = true

    this.editPostDataForm = new FormGroup({
      postName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      categoryName: new FormControl('', Validators.required),
      url: new FormControl(''),
      description: new FormControl('', [Validators.required, Validators.minLength(10)]),
    });

    this.editPostImgForm = new FormGroup({
      imgFile: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
    this.categoryService.getAllCategories()
      .subscribe({
        next: (data) => {
          this.categories = data;
        },
        error: (error) => {
          throwError(error);
        }
      });

    this.postPayload = {
      postName: '',
      url: '',
      description: '',
      categoryName: '',
    };
  }

  get postName() {
    return this.editPostDataForm!.get('postName');
  }

  get categoryName() {
    return this.editPostDataForm!.get('categoryName');
  }

  get url() {
    return this.editPostDataForm!.get('url');
  }

  get description() {
    return this.editPostDataForm!.get('description');
  }

  get imgFile() {
    return this.editPostImgForm!.get('imgFile');
  }

  onLoad() {
    this.loading = false;
  }

  onFileChanged(event: any) {
    this.selectedFile = event.target.files[0];
  }

  goToPost(id: number): void {
    this.router.navigate(['/view-post/' + id])
      .catch(e => console.log(e));
  }

  showDeletePostModal(post: PostModel,) {
    this.postToDelete = post;
    this.showDialogDelete = !this.showDialogDelete
  }

  showEditPostDataModal(post: PostModel) {
    this.editPostDataForm.get("postName")?.setValue(post.postName)
    this.editPostDataForm.get("url")?.setValue(post.url);
    this.editPostDataForm.get("description")?.setValue(post.description)
    this.postToUpdate = post;
    this.showDialogEditData = !this.showDialogEditData
  }

  showEditPostImgModal(post: PostModel) {
    this.postToUpdate = post;
    this.showDialogEditImg = !this.showDialogEditImg
  }

  toggle(btn: any) {
    btn.click()
  }

  toggleFileSelect(event: Event, fileInputEl: HTMLInputElement) {
    event.preventDefault();
    fileInputEl.click();
  }

  deletePost(): void {
    let isConfirmed: boolean = window.confirm("Are you sure to delete this post");

    if (isConfirmed) {
      this.postService.deletePost(this.postToDelete!.id)
        .subscribe({
          next: () => {
          },
          error: (e) => console.log(e)
        });

      this.toastr.success("Post deleted successfully");
      this.reloadComponent();
    }

    this.showDialogDelete = !this.showDialogDelete;
  }

  editPostData(event: Event): void {
    event.preventDefault();
    this.postPayload!.postName = this.editPostDataForm!.get('postName')!.value;
    this.postPayload!.categoryName = this.editPostDataForm!.get('categoryName')!.value;
    this.postPayload!.url = this.editPostDataForm!.get('url')!.value;
    this.postPayload!.description = this.editPostDataForm!.get('description')!.value;

    if (
      !this.postPayload!.postName ||
      !this.postPayload!.categoryName ||
      !this.postPayload!.description) {

      this.toastr.error("Post not edited, please fill all fields");
      return;
    }

    this.postService.editPostData(this.postToUpdate!.id, this.postPayload!)
      .subscribe({
        next: () => {
        },
        error: (e) => console.log(e)
      })

    this.toastr.success("Post updated successfully");
    this.showDialogEditData = !this.showDialogEditData;
    this.reloadComponent();
  }

  editPostImage(event: Event) {
    event.preventDefault()
    if (!this.selectedFile) {
      this.toastr.error("Image not edited, please select image");
      return;
    }

    this.formData.append('imageFile', this.selectedFile!, this.selectedFile!.name);

    this.postService.editPostImg(this.postToUpdate!.id, this.formData)
      .subscribe({
        next: () => {
        },
        error: (error) => {
          throwError(error);
        }
      });

    this.toastr.success("Post updated successfully");
    this.showDialogEditImg = !this.showDialogEditImg;
    this.reloadComponent();
  }

  reloadComponent(): void {
    let currentUrl = this.router.url;

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl])
      .catch(e => console.log(e));
  }

  discardEditingData() {
    this.showDialogEditData = !this.showDialogEditData;
  }

  discardEditingImage(event: Event) {
    event.preventDefault();
    this.showDialogEditImg = !this.showDialogEditImg;
  }

  imgToShowSrc: string | undefined;

  displayImage(event: Event, el: HTMLImageElement) {
    this.showDialogImageOnClick = !this.showDialogImageOnClick;
    console.log(event);
    console.log(el.src);
    this.imgToShowSrc = el.src;
  }

}
