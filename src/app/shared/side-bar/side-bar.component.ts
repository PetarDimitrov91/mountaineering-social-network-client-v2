import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css'],
  providers: [AuthService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideBarComponent {

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {
  }

  get isUserAdmin(): boolean {
    return this.authService.isAdmin;
  }


  goToCreatePost() {
    this.router.navigateByUrl('/create-post')
      .catch(e => console.log(e));
  }

  goToCreateCategory() {
    this.router.navigateByUrl('/create-category')
      .catch(e => console.log(e));
  }

}
