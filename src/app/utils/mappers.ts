import {PostModel} from "../shared/interfaces/post-model";

export function mapPostDto(post:PostModel):object{
  let base64Data:[] = post.imgByte;
  let retrievedImage ='data:image/jpeg;base64,' + base64Data;

  return{
    id: post.id,
    postName: post.postName,
    url: post.url,
    description: post.description,
    likesCount: post.likesCount,
    userName: post.userName,
    categoryName: post.categoryName,
    commentCount: post.commentCount,
    duration: post.duration,
    like: post.like,
    unlike: post.unlike,
    retrievedImage: retrievedImage
  }
}
