export class CommentPayload{
    text: string | undefined;
    postId: number | undefined;
    userName?:string;
    createdDate?: string;
}
