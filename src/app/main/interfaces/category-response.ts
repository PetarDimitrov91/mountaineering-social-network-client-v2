export class CategoryModel {
    id?: number;
    name: string | undefined;
    description: string | undefined;
    numberOfPosts?: number;
}
