export interface PostPayload {
  postName: string | undefined;
  categoryName?: string;
  url?: string;
  description: string | undefined;
}
