import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PostModel} from '../shared/interfaces/post-model';
import {Observable} from 'rxjs';
import {PostPayload} from './interfaces/post.payload';
import {environment} from '../../environments/environment';

const API_URL = environment.API_URL;

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) {
  }

  getAllPosts(): Observable<Array<PostModel>> {
    return this.http.get<Array<PostModel>>(`${API_URL}/api/posts`);
  }

  getAllPostsOrderedByPostName(): Observable<Array<PostModel>> {
    return this.http.get<Array<PostModel>>(`${API_URL}/api/posts/sort?order_by=post_name`);
  }

  getAllPostsOrderedByCategory(): Observable<Array<PostModel>> {
    return this.http.get<Array<PostModel>>(`${API_URL}/api/posts/sort?order_by=category`);
  }

  getAllPostsOrderedByCreatedDateAsc(): Observable<Array<PostModel>> {
    return this.http.get<Array<PostModel>>(`${API_URL}/api/posts/sort?order_by=created_date_asc`);
  }

  getAllPostsOrderedByCreatedDateDesc(): Observable<Array<PostModel>> {
    return this.http.get<Array<PostModel>>(`${API_URL}/api/posts/sort?order_by=created_date_desc`);
  }


  createPost(formData: FormData): Observable<any> {
    return this.http.post(`${API_URL}/api/posts/`, formData);
  }

  getPost(id: number): Observable<PostModel> {
    return this.http.get<PostModel>(`${API_URL}/api/posts/` + id);
  }

  getAllPostsByUser(name: string): Observable<PostModel[]> {
    return this.http.get<PostModel[]>(`${API_URL}/api/posts/by-user/` + name);
  }

  getAllPostsByCategoryId(id: number): Observable<PostModel[]> {
    return this.http.get<PostModel[]>(`${API_URL}/api/posts/by-category/` + id);
  }

  deletePost(id: number) {
    return this.http.delete(`${API_URL}/api/posts/delete?post_id=${id}`);
  }

  editPostData(postId: number, post: PostPayload) {
    return this.http.put(`${API_URL}/api/posts/edit_data?post_id=${postId}`, post);
  }

  editPostImg(postId: number, formData: FormData) {
    return this.http.put(`${API_URL}/api/posts/edit_img?post_id=${postId}`, formData);
  }
}
