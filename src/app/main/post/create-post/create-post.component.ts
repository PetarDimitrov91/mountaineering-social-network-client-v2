import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {CategoryModel} from 'src/app/main/interfaces/category-response';
import {Router} from '@angular/router';
import {PostService} from 'src/app/main/post.service';
import {CategoryService} from 'src/app/main/category.service';
import {throwError} from 'rxjs';
import {PostPayload} from '../../interfaces/post.payload';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  createPostForm: FormGroup;
  postPayload: PostPayload;
  categories: Array<CategoryModel> | undefined;
  selectedFile: File | undefined;
  formData = new FormData();

  constructor(private router: Router, private postService: PostService,
              private categoryService: CategoryService) {
    this.postPayload = {
      postName: '',
      url: '',
      description: '',
      categoryName: '',
    };

    this.createPostForm = new FormGroup({
      postName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      categoryName: new FormControl('', Validators.required),
      url: new FormControl(''),
      description: new FormControl('', [Validators.required, Validators.minLength(10)]),
      imgFile: new FormControl('',Validators.required)
    });
  }

  get postName() {
    return this.createPostForm!.get('postName');
  }

  get categoryName() {
    return this.createPostForm!.get('categoryName');
  }

  get url() {
    return this.createPostForm!.get('url');
  }

  get description() {
    return this.createPostForm!.get('description');
  }

  get imgFile() {
    return this.createPostForm!.get('imgFile');
  }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .subscribe({
        next: (data) => {
          this.categories = data;
        },
        error: (error) => {
          throwError(error);
        }
      });
  }

  onFileChanged(event: any) {
    this.selectedFile = event.target.files[0];
  }

  createPost() {
    this.postPayload.postName = this.createPostForm!.get('postName')!.value;
    this.postPayload.categoryName = this.createPostForm!.get('categoryName')!.value;
    this.postPayload.url = this.createPostForm!.get('url')!.value;
    this.postPayload.description = this.createPostForm!.get('description')!.value;

    this.formData.append('imageFile', this.selectedFile!, this.selectedFile?.name);
    this.formData.append('postRequest', new Blob([JSON.stringify(this.postPayload)], {type: 'application/json'}));

    this.postService.createPost(this.formData)
      .subscribe({
        next: (data) => {
          this.router.navigate(['/home'])
            .catch(e => console.log(e));
        }, error: (error) => {
          throwError(error);
        }
      });
  }

  discardPost() {
    this.router.navigate(['/home']).catch(e => console.log(e));
  }

}
