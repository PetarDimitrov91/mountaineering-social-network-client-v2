import { Component, OnInit} from '@angular/core';
import {PostService} from 'src/app/main/post.service';
import {ActivatedRoute} from '@angular/router';
import {PostModel} from 'src/app/shared/interfaces/post-model';
import {forkJoin, throwError} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommentPayload} from 'src/app/main/interfaces/comment.payload';
import {CommentService} from 'src/app/main/comment.service';
import {AuthService} from '../../../auth/auth.service';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css'],
})
export class ViewPostComponent implements OnInit {
  postId: number | undefined;
  post: PostModel | undefined;
  postImg:any;
  commentForm: FormGroup;
  commentPayload: CommentPayload;
  comments: CommentPayload[] | undefined;
  isLoggedIn: boolean;

  constructor(
    private authService: AuthService,
    private postService: PostService,
    private activateRoute: ActivatedRoute,
    private commentService: CommentService,
  ) {
    this.isLoggedIn = this.authService.isLoggedIn;

    this.commentForm = new FormGroup({
      text: new FormControl('', Validators.required)
    });

    this.commentPayload = {
      text: '',
      postId: undefined
    };
  }

  ngOnInit(): void {
    this.activateRoute.params
      .subscribe((params: any) => {
      this.postId = params.id;

      if(params.id){
        this.commentPayload.postId = params.id;

        forkJoin([
          this.postService.getPost(this.postId!),
          this.commentService.getAllCommentsForPost(this.postId!)
        ])
          .subscribe(([postRes,commentRes]) => {
            this.post = postRes;

            let base64Data:[] = this.post.imgByte;
            this.postImg ='data:image/jpeg;base64,' + base64Data;

            this.comments = commentRes;
          });

      }
    });
  }

  postComment() {
    this.commentPayload.text = this.commentForm!.get('text')!.value;
    this.commentService.postComment(this.commentPayload)
      .subscribe({next:(data) => {
      this.commentForm!.get('text')!.setValue('');
      this.getCommentsForPost();
    }, error: (error) => {
      throwError(error);
    }});
  }

  private getCommentsForPost() {
    this.commentService.getAllCommentsForPost(this.postId!)
      .subscribe({next:(data) => {
      this.comments = data;
    }, error:(error) => {
      throwError(error);
    }});
  }
}
