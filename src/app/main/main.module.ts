import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {CategoryService} from './category.service';
import {CommentService} from './comment.service';
import {PostService} from './post.service';
import {CategoryPostsComponent} from './category/category-posts/category-posts.component';
import {CreateCategoryComponent} from './category/create-category/create-category.component';
import {HomeComponent} from './home/home.component';
import {CreatePostComponent} from './post/create-post/create-post.component';
import {ViewPostComponent} from './post/view-post/view-post.component';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import {GetStartedComponent} from './get-started/get-started.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    GetStartedComponent,
    CategoryPostsComponent,
    CreateCategoryComponent,
    HomeComponent,
    CreatePostComponent,
    ViewPostComponent,
    UserProfileComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers:[
    CategoryService,
    CommentService,
    PostService
  ],
  exports:[
  ]
})
export class MainModule { }
