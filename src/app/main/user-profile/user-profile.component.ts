import {
  Component,
  OnInit,
} from '@angular/core';
import {PostService} from 'src/app/main/post.service';
import {ActivatedRoute} from '@angular/router';
import {CommentService} from 'src/app/main/comment.service';
import {CommentPayload} from 'src/app/main/interfaces/comment.payload';
import {mapPostDto} from "../../utils/mappers";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  username: string;
  posts: Array<any> | undefined;
  comments: Array<CommentPayload> | undefined;
  postCount: number | undefined;
  commentCount: number | undefined;
  profileContext: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
    private commentService: CommentService) {

    this.profileContext = true;
    this.username = this.activatedRoute.snapshot.params['name'];
  }

  private loadData(): void {

    forkJoin([
      this.postService.getAllPostsByUser(this.username)
      , this.commentService.getAllCommentsByUser(this.username)
    ])
      .subscribe(([postsRes,commentRes]) => {
       let mappedPosts = postsRes.map(mapPostDto);

        this.posts = mappedPosts;
        this.postCount = mappedPosts.length;
        this.comments = commentRes;
        this.commentCount = commentRes.length;
    });
  }

  ngOnInit() {
   setTimeout(() => {
      this.loadData()
    },1000);


  }


}
