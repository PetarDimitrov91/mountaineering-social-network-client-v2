import {Component,} from '@angular/core';
import {PostService} from '../post.service';
import {map} from "rxjs/operators";
import {mapPostDto} from "../../utils/mappers";
import {Observable} from "rxjs";
import {PostModel} from "../../shared/interfaces/post-model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {

  posts: Array<any> | undefined;

  constructor(private postService: PostService) {
    this.fetchReq(this.postService.getAllPosts());
  }

  private fetchReq(service: Observable<Array<PostModel>>): void {
    this.posts = undefined;
    service
      .pipe(map((post) => {
        return post.map(mapPostDto)
      }))
      .subscribe((post) => {
        this.posts = post;
      });
  }

  getAllPostsOrderedByPostName(): void {
    this.fetchReq(this.postService.getAllPostsOrderedByPostName());
  }

  getAllPostsOrderedByCategory(): void {
    this.fetchReq(this.postService.getAllPostsOrderedByCategory());
  }

  getAllPostsOrderedByCreatedDateAsc(): void {
    this.fetchReq(this.postService.getAllPostsOrderedByCreatedDateAsc());
  }

  getAllPostsOrderedByCreatedDateDesc(): void {
    this.fetchReq(this.postService.getAllPostsOrderedByCreatedDateDesc());
  }
}
