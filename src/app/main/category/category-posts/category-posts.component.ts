import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PostModel} from '../../../shared/interfaces/post-model';
import {ActivatedRoute} from '@angular/router';
import {PostService} from '../../post.service';
import {map} from "rxjs/operators";
import {mapPostDto} from "../../../utils/mappers";

@Component({
  selector: 'app-category-posts',
  templateUrl: './category-posts.component.html',
  styleUrls: ['./category-posts.component.css'],
})
export class CategoryPostsComponent implements OnInit {
  id: number | undefined;
  posts: Array<any> = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: any) => {
      this.id = params.id;
      if (params.id) {
        this.postService.getAllPostsByCategoryId(this.id!)
          .pipe(map((post) => {
          return post.map(mapPostDto)
        }))
          .subscribe((post) => {
            this.posts = post;
          });
      }
    });
  }
}
