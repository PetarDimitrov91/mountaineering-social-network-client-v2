/* tslint:disable:no-trailing-whitespace */
import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {CategoryModel} from '../../interfaces/category-response';
import {Router} from '@angular/router';
import {CategoryService} from '../../category.service';
import {throwError} from 'rxjs';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent{
  createCategoryForm: FormGroup;
  categoryModel: CategoryModel;
  title = new FormControl('');
  description = new FormControl('');

  constructor(private router: Router, private categoryService: CategoryService) {
    this.createCategoryForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

    this.categoryModel = {
      name: '',
      description: ''
    };
  }

  discard() {
    this.router.navigate(['/home'])
      .catch(e => console.log(e));
  }

  createCategory() {
    this.categoryModel.name = this.createCategoryForm!.get('title')!.value;
    this.categoryModel.description = this.createCategoryForm!.get('description')!.value;
    this.categoryService.createCategory(this.categoryModel)
      .subscribe({
        next: () => {
          this.router.navigateByUrl('/home')
            .catch(e => console.log(e));
        },
        error: (error) => {
          throwError(error);
        }
      });
  }
}
