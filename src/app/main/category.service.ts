import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CategoryModel } from './interfaces/category-response';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';

const API_URL = environment.API_URL;

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }

  getAllCategories(): Observable<Array<CategoryModel>> {
    return this.http.get<Array<CategoryModel>>(`${API_URL}/api/category`);
  }

  createCategory(categoryModel: CategoryModel): Observable<CategoryModel> {
    return this.http.post<CategoryModel>(`${API_URL}/api/category`,
      categoryModel);
  }
}
