import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignupComponent} from './auth/signup/signup.component';
import {LoginComponent} from './auth/login/login.component';
import {HomeComponent} from './main/home/home.component';
import {CreatePostComponent} from './main/post/create-post/create-post.component';
import {CreateCategoryComponent} from './main/category/create-category/create-category.component';
import {ViewPostComponent} from './main/post/view-post/view-post.component';
import {UserProfileComponent} from './main/user-profile/user-profile.component';
import {AuthGuard} from './core/guards/auth.guard';
import {GetStartedComponent} from './main/get-started/get-started.component';
import {CategoryPostsComponent} from './main/category/category-posts/category-posts.component';

const routes: Routes = [
  {
    path: '',
    component: GetStartedComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: false,
      authenticationFailureRedirectUrl: '/'
    }
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'view-post/:id',
    component: ViewPostComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: false,
      authenticationFailureRedirectUrl: '/'
    }
  },
  {
    path: 'user-profile/:name',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: true,
      authenticationFailureRedirectUrl: '/login'

    }
  },
  {
    path: 'create-post',
    component: CreatePostComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: true,
      authenticationFailureRedirectUrl: '/login'
    }
  },
  {
    path: 'create-category',
    component: CreateCategoryComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: true,
      authenticationFailureRedirectUrl: '/login'
    }
  },
  {
    path: 'sign-up',
    component: SignupComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: false,
      authenticationFailureRedirectUrl: '/'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: false,
      authenticationFailureRedirectUrl: '/'

    }
  },
  {
    path: 'view-category/:id',
    component: CategoryPostsComponent,
    canActivate: [AuthGuard],
    data: {
      authenticationRequired: false,
      authenticationFailureRedirectUrl: '/'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
