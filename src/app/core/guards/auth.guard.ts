/* tslint:disable:max-line-length no-trailing-whitespace */
import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const {
      authenticationRequired,
      authenticationFailureRedirectUrl
    } = route.data;

    if (route.url.length > 0) {
      if (route.url[0].path == 'user-profile') {
        if (route.url[1].path !== this.authService.getUserName) {
          if (this.authService.getUserName) {
            return this.router.parseUrl('/');
          }
        }
      }
    }

    if (typeof authenticationRequired === 'boolean' && authenticationRequired === this.authService.isLoggedIn) {
      return true;
    }

    if (typeof authenticationRequired === 'boolean' && authenticationRequired === false) {
      return true;
    }

    let authRedirectUrl = authenticationFailureRedirectUrl;

    if (authenticationRequired) {
      const loginRedirectUrl = route.url.reduce((acc, s) => `${acc}/${s.path}`, '');
      authRedirectUrl += `?redirectUrl=${loginRedirectUrl}`;
    }

    return this.router.parseUrl(authRedirectUrl || '/');
  }
}
