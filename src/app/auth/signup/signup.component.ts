import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SignupRequestPayload } from '../interfaces/singup-request.payload';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  signupRequestPayload: SignupRequestPayload;
  signupForm: FormGroup;

  constructor(private authService: AuthService, private router: Router,
    private toastr: ToastrService) {
    this.signupRequestPayload = {
      username: '',
      email: '',
      password: ''
    };

    this.signupForm = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  signup() {
    this.signupRequestPayload.email = this.signupForm!.get('email')!.value;
    this.signupRequestPayload.username = this.signupForm!.get('username')!.value;
    this.signupRequestPayload.password = this.signupForm?.get('password')!.value;

    this.authService.signup(this.signupRequestPayload)
      .subscribe({
        next: () => {
          this.router.navigate(['/login'],
            { queryParams: { registered: 'true' } })
            .catch(e => console.log(e));
        },
        error: () =>{
          this.toastr.error('Registration Failed! Please try again');
        }
    });
  }
}
