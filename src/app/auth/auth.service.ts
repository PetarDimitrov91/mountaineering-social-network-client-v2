/* tslint:disable:no-trailing-whitespace */
import {Injectable, Output, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SignupRequestPayload} from './interfaces/singup-request.payload';
import {Observable, throwError} from 'rxjs';
import {LocalStorageService} from 'ngx-webstorage';
import {LoginRequestPayload} from './interfaces/login-request.payload';
import {LoginResponse} from './interfaces/login-response.payload';
import {map, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {RefreshTokenPayload} from './interfaces/refresh-token-payload';
import {Router} from '@angular/router';


const API_URL = environment.API_URL;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  @Output() loggedIn: EventEmitter<boolean> = new EventEmitter();
  @Output() username: EventEmitter<string> = new EventEmitter();
  private hasCurrentUser: boolean;

  refreshTokenPayload: RefreshTokenPayload = {
    refreshToken: '',
    username: ''
  };

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private localStorage: LocalStorageService) {
    this.hasCurrentUser = false;
  }

  signup(signupRequestPayload: SignupRequestPayload): Observable<any> {
    return this.httpClient.post(`${API_URL}/api/auth/signup`, signupRequestPayload, {responseType: 'text'});
  }

  login(loginRequestPayload: LoginRequestPayload): Observable<boolean> {
    return this.httpClient.post<LoginResponse>(`${API_URL}/api/auth/login`,
      loginRequestPayload).pipe(map(data => {
      this.localStorage.store('authenticationToken', data.authenticationToken);
      this.localStorage.store('username', data.username);
      this.localStorage.store('refreshToken', data.refreshToken);
      this.localStorage.store('expiresAt', data.expiresAt);
      this.localStorage.store('isAdmin', data.admin);

      this.setRefreshTokenPayloadRefreshToken(data.refreshToken);
      this.setRefreshTokenPayloadUsername(data.username);

      this.loggedIn.emit(true);
      this.username.emit(data.username);
      this.hasCurrentUser = true;

      return true;
    }));
  }

  get getJwtToken(): string {
    return this.localStorage.retrieve('authenticationToken');
  }

  refreshToken() {
    if (!this.refreshTokenPayload.refreshToken || !this.refreshTokenPayload.username) {
      const refreshToken: string | undefined = this.localStorage.retrieve('refreshToken');
      const username: string | undefined = this.localStorage.retrieve('username');

      this.setRefreshTokenPayloadRefreshToken(refreshToken);
      this.setRefreshTokenPayloadUsername(username);
    }

    return this.httpClient.post<LoginResponse>(`${API_URL}/api/auth/refresh/token`,
      this.refreshTokenPayload)
      .pipe(tap(response => {
        this.localStorage.clear('authenticationToken');
        this.localStorage.clear('expiresAt');

        this.localStorage.store('authenticationToken',
          response.authenticationToken);
        this.localStorage.store('expiresAt', response.expiresAt);
      }));
  }

  logout() {
    if (!this.refreshTokenPayload.refreshToken || !this.refreshTokenPayload.username) {
      const refreshToken: string | undefined = this.localStorage.retrieve('refreshToken');
      const username: string | undefined = this.localStorage.retrieve('username');

      if (!refreshToken || !username) {
        this.clearLocalStorage();
        return;
      }

      this.setRefreshTokenPayloadRefreshToken(refreshToken);
      this.setRefreshTokenPayloadUsername(username);
    }

    this.httpClient.post(`${API_URL}/api/auth/logout`, this.refreshTokenPayload,
      {responseType: 'text'})
      .subscribe(
        {
          next: (message) => {
            console.log(message);
          },
          error: (error) => {
            this.hasCurrentUser = false;
            this.clearLocalStorage();
            throwError(error);
          }
        });

    this.hasCurrentUser = false;
    this.clearLocalStorage();
  }

  private clearLocalStorage(): void {
    this.localStorage.clear('authenticationToken');
    this.localStorage.clear('username');
    this.localStorage.clear('refreshToken');
    this.localStorage.clear('expiresAt');
    this.localStorage.clear('isAdmin');
  }

  get getUserName(): string {
    return this.localStorage.retrieve('username');
  }

  get getRefreshToken(): string {
    return this.localStorage.retrieve('refreshToken');
  }

  get isLoggedIn(): boolean {
    return this.getJwtToken != null;
  }

  get isAdmin(): boolean {
    return this.localStorage.retrieve('isAdmin');
  }

  private setRefreshTokenPayloadUsername(username: string | undefined) {
    if (typeof username === "string") {
      this.refreshTokenPayload.username = username;
    }
  }

  private setRefreshTokenPayloadRefreshToken(refreshToken: string | undefined) {
    if (refreshToken != null) {
      this.refreshTokenPayload.refreshToken = refreshToken;
    }
  }
}
