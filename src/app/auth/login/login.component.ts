/* tslint:disable:no-trailing-whitespace */
import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {LoginRequestPayload} from '../interfaces/login-request.payload';
import {AuthService} from '../auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {throwError} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginRequestPayload: LoginRequestPayload;
  registerSuccessMessage: string | undefined;
  isError: boolean | undefined;
  loginSucceeded: boolean | undefined;

  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute,
              private router: Router, private toastr: ToastrService) {
    this.loginRequestPayload = {
      username: '',
      password: ''
    };

    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
    this.loginSucceeded = false;

    this.activatedRoute.queryParams
      .subscribe(params => {
        if (params['registered'] !== undefined && params['registered'] === 'true') {
          this.toastr.success('Registration Successful');
          this.registerSuccessMessage = 'Please Check your inbox for activation email '
            + 'activate your account before you Login!';
        }
      });
  }

  login() {
    this.loginRequestPayload.username = this.loginForm!.get('username')!.value;
    this.loginRequestPayload.password = this.loginForm!.get('password')!.value;

    this.authService.login(this.loginRequestPayload).subscribe({
      next: () => {
        this.isError = false;

        const redirectUrl = this.activatedRoute.snapshot.queryParams['redirectUrl'] || '/home';

        this.router.navigate([redirectUrl]).catch(e => console.log(e.message));
        this.loginSucceeded = true;
        this.toastr.success('Login Successful');
      },
      error: (err) =>{
        this.isError = true;
        throwError(err);
      }});
  }
}
