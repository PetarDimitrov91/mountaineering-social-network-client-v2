# Mountaineering Social Network
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.1.

The project is a part from the final assignment from the Software University's Angular course and also the 
last assignment in the Software University's Web Development Professional Module.

<hr>

## Project information
For this project, I created also a REST Service, based on Java Spring Framework.
You can explore the Rest Service repository just by flowing this link 
<button>[Repository: Mountaineering-REST-API]</button> 


The boot projects are also deployed on Heroku.

Mountaineering App-Client: [![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://mountain-app-client.herokuapp.com/home)

The rest api is documented with Swagger UI, which can be accessed from the button below.

Mountaineering App-REST-API: [![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://mountain-app-rest-api.herokuapp.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)


## Usage

## Deployed version

I recommend using the deployed version of the application.
You will be not able to register a new user because when the user is registered, a verification email will be sent from the back-end.
You will not be able to activate the account because I am using the mailtrap.io as an email service for receiving mails for every user.

You can simply use these credentials to make a successful login:

Admin user:\
username: petar\
password: 123456

Normal user:\
username: ivan\
password: 123456


## Local usage
You can also clone the both repos and run the application locally.

For the mountaineering-app-client just use your terminal, navigate to the project directory and simply write the following commands.

`npm i`
`ng serve`

For the mountaineering-app-rest-api the things are not so simple. You must configure your own mailtrap.io account.\
Just visit the following link [how to set up mailtrap.io with spring]

## Maintainer
[Petar Dimitrov]

## License
[MIT]

---
[Petar Dimitrov]: https://github.com/PetarDimitrov91
[MIT]: https://choosealicense.com/licenses/mit/
[Repository: Mountaineering-REST-API]: https://gitlab.com/PetarDimitrov91/mountain_app-rest_api
[Mountaineering-app client]: https://mountain-app-client.herokuapp.com/home
[Mountaineering-app REST-API]: https://mountain-app-rest-api.herokuapp.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
[how to set up mailtrap.io with spring]: https://medium.com/@.aj/spring-boot-thymeleaf-mailtrap-io-790519c72bfd















